**安装:**
> wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh"  
chmod +x tcp.sh  
./tcp.sh

**注意:**
> 支持系统
Centos 6+ / Debian 7+ / Ubuntu 14+
BBR魔改版不支持Debian 8

**参考资料**  
魔改BBR原帖：http://www.hostloc.com/thread-372277-1-2.html

脚本参考：https://ylws.me/tech/68.html

技术参考：http://51.ruyo.net/p/4415.html

同时非常感谢vicer提供Lotserver一键脚本。
